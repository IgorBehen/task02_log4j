package com.ihorbehen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static final Logger log = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        log.info("Info");
        log.trace("Trace");
        log.debug("Debug");
        log.warn("Warn");
        log.error("Error");
        log.fatal("Fatal");

    }
}
